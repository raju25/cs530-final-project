import React from 'react';
import {
    BrowserRouter,
    Switch,
    Route,
} from "react-router-dom";
import Cocktail from './Cocktail';
import App from './../App';
import Mocktail from './Mocktail';
import Drink from './Drink';
import Search from './Search';
import Brewery from './Brewery';
function Router() {
    return (
        <BrowserRouter>
            <Switch>
                <Route 
                    exact 
                    path="/" 
                    component={App} 
                />
                <Route 
                    path="/mocktail" 
                    component={Mocktail} 
                />
                <Route
                    path="/cocktail"
                    component={Cocktail}
                />
                <Route
                    path="/drinkDetails/id:drink"
                    component={Drink}
                />
                <Route
                    path="/search/drink:cocktail"
                    component={Search}
                />
                <Route
                    path="/location/city:city"
                    component={Brewery}
                />
            </Switch>
        </BrowserRouter>
    );
}

export default Router;
