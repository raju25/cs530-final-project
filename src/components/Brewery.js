import React,{useEffect,useState} from 'react'
import axios from 'axios'
const Brewery = () => {
  const [breweries,setBreweries]=useState([])
  const city=window.location.pathname.split("=")[1]
  useEffect(()=>{
    const getBreweryList=async()=>{
      const {data}=await axios.get(`https://api.openbrewerydb.org/breweries?by_city=${city}`)
      setBreweries(data)
    }
    getBreweryList()
  },[city])
  const breweryList=breweries.map((data,idx)=>(
    <tr key={idx}>
    <th scope='row'>{idx+1}</th>
    <td>{data.name}</td>
    <td>{data.brewery_type}</td>
    <td>{data.street}</td>
    <td>{data.city}</td>
    <td>{data.state}</td>
    </tr>

  ))
  console.log(breweries)
  return (
    <div>
    <header className="App-header">
      <h2 className="App-title">Breweries </h2>
    </header>
      <div className='container'>
        <table className='table table-dark'>
          <thead className='thead-dark'>
          <tr>
          <th scope="col">#</th>
          <th scope="col">Brewery Name</th>
          <th scope="col">Brewery Type</th>
          <th scope="col">Street</th>
          <th scope="col">City</th>
          <th scope="col">State</th>
          </tr>
          </thead>
          <tbody>
          {breweryList}
          </tbody>
        </table>
      </div>
    </div>
  )
}

export default Brewery
