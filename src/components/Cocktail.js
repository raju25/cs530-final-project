import React,{useEffect,useState} from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faGlassMartini } from '@fortawesome/free-solid-svg-icons'
import axios from "axios"
import '../App.css'
const Cocktail = () => {
  const [drink,setDrink]=useState([])
  const [choice,setChoice]=useState("")
  useEffect(()=>{
    const getCocktails=async()=>{
      const {data}=await axios.get("https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=Alcoholic")
      setDrink(data.drinks)
    }
    getCocktails();
  })
  console.log("Drinks : ",drink)
  const handleChange=(e)=>{
    setChoice(e.target.value);
  }
  const menuList=drink.map((data)=>(
    <div key={data.idDrink} className='card'>
      <img src={data.strDrinkThumb} className="card-img-top" alt={data.idDrink}/>
      <h5 className="card-title">{data.strDrink}</h5>
      <a href={`/drinkDetails/id=${data.idDrink}`}><button className='btn btn-warning'>View</button></a>
    </div>

   
  ))
  return (
    <div>
      <div className='container drinkMenu'>
          <h3>Welcome to Cocktail Menu <a href="/"><button className='btn btn-primary'>Home</button></a></h3>
          <input 
            placeholder='Find your favourite'
            value={choice}
            onChange={handleChange}
          />
          <a href={`/search/drink=${choice}`}><button className='btn btn-danger'><FontAwesomeIcon icon={faGlassMartini}/></button></a>
          <div className='row'>
          <div className='col-md-3'>
          {menuList}
          </div>
          </div>
          
      </div>
     
    </div>
  )
}

export default Cocktail
