import React from 'react';
import './App.css';
import Home from './components/Home';
//hello food//
class App extends React.Component {
  constructor(props){
    super(props);
 
    this.state = {
      recipes: []
    };
  } 

  
  render(){
    return (
      <div className="App">
        <header className="App-header">
          <h2 className="App-title">Welcome to Cocktail Bar</h2>
        </header>
        <Home/>
      </div>    
    );
  }
}

export default App;
