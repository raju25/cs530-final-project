import React, { useEffect,useState } from 'react'
import axios from 'axios'
import "../App.css"
const Drink = () => {
    const drinkID=window.location.pathname.split("=")[1]
    const [drinkDetail,setDrinkDetail]=useState({})
    let ingList=[]
    useEffect(()=>{
       const getDrinkDetails=async(drinkID)=>{
            const {data}=await axios.get(`https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=${drinkID}`)
         
            setDrinkDetail(data.drinks[0])
       }
      
       getDrinkDetails(drinkID)
    },[drinkID])
    for(let i=1;i<10;i++){
        let ing=`strIngredient${i}`
        console.log(drinkDetail[ing])
        if(drinkDetail[ing]!=null){
            ingList.push(drinkDetail[ing])
        }
    }
    console.log("Drink Details :",drinkDetail)
    console.log(ingList)
    const listValues=ingList.map((data,idx)=>(
        <li key={idx}>{data}</li>
    ))
  
  return (
    <div>
      <div className='container'>
        <div className='card detailDrink'>
        <img src={drinkDetail.strDrinkThumb} alt={drinkDetail.idDrink}/>
        <div class="card-body">
    <h5 class="card-title">{drinkDetail.strDrink}</h5>
    <h6>Ingredients</h6>
    <ul>
    {listValues}
    </ul>
    <p class="card-text">{drinkDetail.strInstructions}</p>
    <a href="/" class="btn btn-primary">Home</a>
  </div>
       
        </div>
      </div>
    </div>
  )
}

export default Drink
