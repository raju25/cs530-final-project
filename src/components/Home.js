import React,{useState} from 'react'

const Home = () => {
  const [city,setCity]=useState('');
  
  const handleChange=(e)=>{
    setCity(e.target.value)
  }
  return (
    <div>
      
      <div className='container' style={{marginTop:"15%"}}>
      <a href="/mocktail"><button className='btn btn-success'>Our Mocktails</button></a>
      <a href="/cocktail"><button className='btn btn-danger'>Our Cocktails</button></a><br/>
      <input 
        type="text"
        placeholder='Enter the City Name'
        value={city}
        onChange={handleChange}
      /><br/>
      <a href={`/location/city=${city}`}><button className='btn btn-warning'>Search</button></a><br/>
      <br/>
      

      </div>
    

    </div>
  )
}

export default Home
