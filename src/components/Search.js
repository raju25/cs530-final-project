import React,{useEffect,useState} from 'react'
import axios from 'axios'
const Search = () => {
    const drink=window.location.pathname.split("=")[1]
    console.log("Selected Drink ",drink)
    const [drinkList,setDrinkList]=useState([])

    useEffect(()=>{
        const getDrink=async()=>{
            const url=`https://www.thecocktaildb.com/api/json/v1/1/search.php?s=${drink}`
            const {data}=await axios.get(url)
            setDrinkList(data.drinks)
        }

        getDrink()
    },[drink])
    

    
    const drinkOptions=drinkList.map((data)=>(
        <div className='container'>
            <div className='card detailDrink'>
            <img src={data.strDrinkThumb} className="card-img-top" alt={data.idDrink}/>
            <div class="card-body">
            <h4 class="card-title">{data.strDrink}</h4>

            <h6 className="card-title">Procedure:</h6>
            <p class="card-text">{data.strInstructions}</p>
            <a href="/" class="btn btn-primary">Home</a><a href={`/drinkDetails/id=${data.idDrink}`}><button className='btn btn-warning'>View in Detail</button></a>
          </div>
            </div>
        </div>
    ))
  return (
    <div>
      <div className='row'>
          <div className='col-md-3'>
          {drinkOptions}
          </div>
        </div>
    </div>
  )
}

export default Search
